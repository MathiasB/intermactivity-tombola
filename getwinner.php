<?php 
require_once('core/main.php');

$m = new Tombola();


foreach ($m->getWinners() as $winnerType) {
    
    foreach ($winnerType as $winner) { 
        
        echo '<li>';
        echo $winner['firstname'];
        echo ' ';
        echo $winner['lastname'];
        
        if($winner['nickname'] != '##') {
        
            echo ' - ';
            echo $winner['nickname'];
            
        }
        
        echo '</li>';
        
    }
    
}

?>