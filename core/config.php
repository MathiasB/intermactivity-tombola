<?php 

//Database information
define('DB_SERVER', 'localhost');
define('DB_NAME', 'IM');
define('DB_USER_NAME', 'root');
define('DB_USER_PASSWORD', 'root');

define('DB_PARTICIPANTS', 'deelnemers'); //Table where the participants are stored


//Tombala informations
define('WINNERS_MEMBER', 5);
define('WINNERS_NON_MEMBER', 1);

//Variable to prevent of executing installation more than once
//Must be set to 'true' after installation!!!
define('INSTALLED', false);

//Dummy value for non members, this value will be placed in the place of the userName
define('NON_MEMBER_VALUE', '##');

//Footer/Copyright message
define('FOOTER', 'Copyright © 2013 IM.be - Alle rechten voorbehouden');
?>