<?php
/*
Datastructure for Participant

Author: Mathias Beke
Url: http://denbeke.be
Date: November 2013
*/


/**
@brief Datastructure for Participant

Store the following information:
- first name
- last name
- email
- nickname
*/
class Participant {

    public $firstName;
    public $lastName;
    public $email;
    public $nickName;
    
    
    /**
    Constructor
    
    @param first name
    @param last name
    @param email
    @param nickname
    */
    public function __construct($firstName, $lastName, $email, $nickName) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->nickName = $nickName;
    }

}

?>