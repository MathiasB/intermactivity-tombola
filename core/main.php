<?php 
/*
Main functions for InterMactivity Tombola

Author: Mathias Beke
Url: http://denbeke.be
Date: November 2013
*/


require_once(dirname(__FILE__).'/config.php');
require_once(dirname(__FILE__).'/database.php');
require_once(dirname(__FILE__).'/participant.php');


/**
@brief Main functions for InterMactivity Tombola
*/
class Tombola {
    
    private $database;
    
    
    /**
    Constructor
    Initialize database object for further use
    */
    public function __construct() {
        $this->database = new Database(DB_SERVER, DB_USER_NAME, DB_USER_PASSWORD, DB_NAME);    
    }
    
    
    /**
    Add participant to the database
    
    @param participant
    @pre participant not yet in database
    @post participant added to the database
    */
    public function addParticipant($participant) {
        //TODO Check if participant yet in database
        $nickName = $this->database->escape($participant->nickName);
        $firstName = $this->database->escape($participant->firstName);
        $lastName = $this->database->escape($participant->lastName);
        $email = $this->database->escape($participant->email);
        
        $query = "INSERT INTO `IM`.`deelnemers` (`id`, `firstname`, `lastname`, `email`, `nickname`) 
        VALUES (NULL, '$firstName', '$lastName', '$email', '$nickName');";
        
        $this->database->doQuery($query);     
    }
    
    
    /**
    Pick winners from the database
    Number of winners must be set in config.php
    
    @return array with winners
    $winners = array(
        'member' => array(
            ..
        ),
        'nonmember' => array(
        )
    );
    @post winners will be written to file
    */
    public function getWinners() {
        
        $table = $this->database->escape(DB_PARTICIPANTS);
        $nonMember = $this->database->escape(NON_MEMBER_VALUE);
        
        //Get the members
        $query = "
        SELECT * 
        FROM  `$table`
        WHERE  `nickname` !=  '$nonMember'";
        
        $members =  $this->database->getQuery($query);
        
        
        //Get the non members
        $query = "
        SELECT * 
        FROM  `$table`
        WHERE  `nickname` =  '$nonMember'";
        
        $nonMembers =  $this->database->getQuery($query);
        
        
        
        //Chose the winners
        $winners = array();
        $winners['member'] = array();
        $winners['nonmember'] = array();
        
        $randomValues = array();    
        
        //Members
        for ($i = 0; $i < WINNERS_MEMBER; $i++) {
            $random = rand(0, sizeof($members)-1);
            if(in_array($random, $randomValues)) {
                $i--;
                continue;
            }
            $randomValues[] = $random;
            $winners['member'][] = $members[$random];     
        }
        
        $randomValues = array(); //Reset the random values
        
        //Non Members
        for ($i = 0; $i < WINNERS_NON_MEMBER; $i++) {
            $random = rand(0, sizeof($nonMembers)-1);
            if(in_array($random, $randomValues)) {
                $i--;
                continue;
            }
            $randomValues[] = $random;
            
            //TODO SOMETIMES NULL!!!
            //BUGFIX NEEDED      !!!
            
            $winners['nonmember'][] = $nonMembers[$random];     
        }
        
        
        //Write winners to file
        $fileName = dirname(__FILE__) . '/../winners/' . 'winners-' . date('d-m-Y') . '-' . time();
        $fileContent = $fileContent . 'Winnaars ' . date('r') . "\n\n";
        $fileContent = $fileContent . "Leden \n";
        
        foreach($winners['member'] as $value) {
            $fileContent = $fileContent . '- ' . $value['firstname'] . ' ' . $value['lastname'] . "\n";
        }
 
        
        
        $fileContent = $fileContent . "\nNiet-Leden \n";
        
        foreach($winners['nonmember'] as $value) {
            $fileContent = $fileContent . '- ' . $value['firstname'] . ' ' . $value['lastname'] . "\n";
        }
        
        file_put_contents($fileName, $fileContent);
        
        return $winners;
        
    }
    
    
}



?>