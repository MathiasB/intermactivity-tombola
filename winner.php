<?php

require_once('core/config.php');

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>InterMactivity Wedstrijd</title>
    
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="style.css" type="text/css" />
    
    <script src="jquery-1.10.2.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
    
    
    $(document).ready(function() {
    
        $('#winner').click(
        
            function() {
            
                setTimeout(function() {
                    $('#winners').html('<h3>3</h3>'); 
                }, 500);
                
                setTimeout(function() {
                    $('#winners').html('<h3>2</h3>'); 
                }, 1500);
                
                setTimeout(function() {
                    $('#winners').html('<h3>1</h3>'); 
                }, 2500);
                
                
                setTimeout(function() {
                    $.get( "getwinner.php", function( data ) {
                        
                        var output = '<ol>';
                        output += data;
                        output += '</ol>';
                        
                        $('#winners').html(output);
                      
                      
                    });
                }, 3500);
                
                
            
            }
        
        );
        
    });
    
    
    
    </script>
    
</head>
<body>


<div id="container">

    <h1>InterMactivity Wedstrijd</h1>
    
    
    <div id="winners">
        <p>
            <a id="winner"><img src="BUTTON-WINNERS.png" alt="En de winnaars zijn..." /></a>
        </p>
    </div>


</div>

<footer>

	<p><?php echo FOOTER; ?></p>

</footer>

    
</body>
</html>