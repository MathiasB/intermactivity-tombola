<?php

require_once('core/config.php');
require_once('core/database.php');


try {

    if(INSTALLED) {
        throw new exception('Installation is already done');
    }

    $database = new Database(DB_SERVER, DB_USER_NAME, DB_USER_PASSWORD, DB_NAME);
    
    $db = $database->escape(DB_NAME);
    $table = $database->escape(DB_PARTICIPANTS);
    
    $query = "CREATE TABLE  `$db`.`$table` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `firstname` VARCHAR( 100 ) NOT NULL ,
    `lastname` VARCHAR( 100 ) NOT NULL ,
    `email` VARCHAR( 100 ) NOT NULL ,
    `nickname` VARCHAR( 100 ) NOT NULL
    ) ENGINE = INNODB;";
    
    $database->doQuery($query);
    
    echo '<h2>Database successfully initialized</h2>';
    
}
catch (exception $e) {
    
    echo '<h2>Error occured while initializing database</h2>';
    echo '<p>' . $e->getMessage() . '<p>';
    
}

?>