<?php

require_once('core/main.php');

$registered = false;
$firstName = "";
$error = "";

if(isset($_GET['registered'])) {

    if($_GET['registered'] == 'true') {
    
    
        try {
        
            if(!isset($_POST['firstName']) or empty($_POST['firstName'])) {
            	throw new exception('Geen voornaam opgegeven');
            }
            if(!isset($_POST['lastName']) or empty($_POST['lastName'])) {
            	throw new exception('Geen achternaam opgegeven');
            }
            if(!isset($_POST['email']) or empty($_POST['email'])) {
            	throw new exception('Geen email adres opgegeven');
            }
    
         	$nickName = htmlspecialchars($_POST['nickName']);
            
            if($nickName == '') {
            	$nickName = '##';
            }
            
            $firstName = htmlspecialchars($_POST['firstName']);
            $lastName = htmlspecialchars($_POST['lastName']);
            $email = htmlspecialchars($_POST['email']);
            
            $p = new Participant($firstName, $lastName, $email, $nickName);    
            $m = new Tombola();
        
            $m->addParticipant($p);
            
            $registered = true;
        
        }
        
        catch (exception $exception) {
            
            $registered = false;
            $error = $exception->getMessage();
            
        }
    
    }

}


?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Doe mee aan de InterMactivity Wedstrijd</title>
    
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="style.css" type="text/css" />
    
    
    <?php 
    
    if($registered) {
   	?>
   	<META HTTP-EQUIV="refresh" CONTENT="5;URL=register.php?registered=false">
   	<?php
    }
    
    ?>
    
</head>
<body>


<div id="container">
	
	<h1>Doe mee aan de InterMactivity Wedstrijd</h1>
	
	
	
	<?php
	
	if($registered) {
	
		echo "<h2 class=\"notice good\">Bedankt voor je deelname, $firstName</h2>";
	
	}
	else {
	
		if($error != '') {
		
			echo "<h2 class=\"notice error\">$error</h2>";
			
		}
	
	?>
	
	
	<form method="post" action="register.php?registered=true">
	
	    <p>
	        <input type="text" name="nickName" value="" placeholder="Gebruikersnaam"/>
	    </p>
	    
	    <p>
	        <input type="text" name="firstName" value="" placeholder="Voornaam"/>
	    </p>
	    
	    <p>
	        <input type="text" name="lastName" value="" placeholder="Achternaam"/>
	    </p>
	    
	    <p>
	        <input type="text" name="email" value="" placeholder="Email"/>
	    </p>
	    
	    <p>
	        <input type="submit" name="submut" value="Verzenden" />
	    </p>
	
	
	</form>
	
	<?php 
	
	}
	
	?>
	
</div>


<footer>

	<p><?php echo FOOTER; ?></p>

</footer>

    
</body>
</html>